package com.worldfirst.trade.app.model;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

import com.worldfirst.trade.app.model.Order;


/**
 * @author Nisha.Vaswani
 *
 */
public class OrderTest
{
	private Order order;


	@Test
	public void should_return_correct_price_when_getPrice()
	{
		order = new Order(new BigDecimal(12.0), 1,"Reliance","TestUser");
		assertEquals(new BigDecimal(12.0), new BigDecimal(12.0));
	} 

	@Test
	public void should_set_correct_price_when_setPrice()
	{
		order = new Order(new BigDecimal(12.0), 1,"Reliance","TestUser");
		order.setPrice(new BigDecimal(15.0));
		assertEquals(new BigDecimal(15.0), order.getPrice());
	}

	@Test
	public void should_return_correct_quantity_when_getQuantity()
	{
		order = new Order(new BigDecimal(12.0), 1,"Reliance","TestUser");
		assertEquals(1, order.getQuantity());

	}

	@Test
	public void should_set_correct_quantity_when_setQuantity()
	{
		order = new Order(new BigDecimal(12.0), 1,"Reliance","TestUser");
		order.setQuantity(5);
		assertEquals(5, order.getQuantity());
	}

	@Test
	public void should_set_correct_companyName_when_setcompanyName()
	{
		order = new Order(new BigDecimal(12.0), 1,"Reliance","TestUser");
		order.setCompanyName("Reliance");;
		assertEquals("Reliance", order.getCompanyName());
	}

	@Test
	public void should_return_correct_companyName_when_getcompanyName()
	{
		order = new Order(new BigDecimal(12.0), 1,"Reliance","TestUser");
		assertEquals("Reliance", order.getCompanyName());
	}


	@Test
	public void should_return_correct_userId_when_getUserId()
	{
		order = new Order(new BigDecimal(12.0), 1,"Reliance","TestUser");
		assertEquals("TestUser", order.getUserId());
	}

	@Test
	public void should_set_correct_userId_when_setUserId()
	{
		order = new Order(new BigDecimal(12.0), 1,"Reliance","TestUser");
		order.setUserId("TestUser");
		assertEquals("TestUser", order.getUserId());
	}

	@Test
	public void should_return_correct_stringFormat_when_toString()
	{
		order = new Order(new BigDecimal(12), 1,"Reliance","TestUser");
		assertEquals("12 1 Reliance TestUser", order.toString());
	}



} 
