package com.worldfirst.trade.app;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Nisha.Vaswani
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeApplicationTests {

	private OrderBook market;

	@Before
	public void initMarket()
	{
			market = new OrderBook("Test");
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void addNewBidShouldCorrectlyAddNewBid2()
	{
			initMarket();
			assertTrue(market.getBidMap().isEmpty());
			market.addBid("TestUser",new BigDecimal(12), 1,"Relaince");
			market.getBidMap();
			assertTrue(market.getBidMap().containsKey(new BigDecimal(12)));
	}

}
