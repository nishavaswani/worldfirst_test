package com.worldfirst.trade.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class OrderBookTest
{
    private OrderBook market;

    @Before
    public void initMarket()
    {
        market = new OrderBook("Test");
    }

    /*******************************************
     *
     *   addBid Tests
     *
     *******************************************/

    @Test
    public void addNewBid_should_correctly_call_AddNewBid()
    {
        initMarket();
        assertTrue(market.getBidMap().isEmpty());
        market.addBid("Test",new BigDecimal(12), 1,"");
        assertTrue(market.getBidMap().containsKey(new BigDecimal(12)));
    }

    @Test
    public void addDuplicateBidPrice_should_correctly_AddNewBid()
    {
        initMarket();
        assertTrue(market.getBidMap().isEmpty());
        market.addBid("TestUser",new BigDecimal(12.0), 1,"Reliance");
        market.addBid("TestUser",new BigDecimal(12.0), 2,"Reliance");

        // Checks to see if corresponding elements have same quantities.
        assertEquals(1, market.getBidMap().get(new BigDecimal(12.0)).get(0).getQuantity());
        assertEquals(2, market.getBidMap().get(new BigDecimal(12.0)).get(1).getQuantity());
    }

    /*******************************************
     *
     *   addOffer Tests
     *
     *******************************************/

    @Test
    public void addNewOffer_should_correctly_AddNewOffer()
    {
        initMarket();
        assertTrue(market.getOfferMap().isEmpty());
        market.addOffer("TestUser",new BigDecimal(12.0), 1,"testCompany");
        assertTrue(market.getOfferMap().containsKey(new BigDecimal(12.0)));
    }

    @Test
    public void addDuplicateOffer_price_should_correctly_AddNewOffer()
    {
        initMarket();
        assertTrue(market.getOfferMap().isEmpty());
        market.addOffer("TestUser",new BigDecimal(12.0), 1,"TestCompany");
        market.addOffer("TestUser",new BigDecimal(12.0), 2,"TestCompany");

        // Checks to see if corresponding elements have same quantities.
        assertEquals(1, market.getOfferMap().get(new BigDecimal(12.0)).get(0).getQuantity());
        assertEquals(2, market.getOfferMap().get(new BigDecimal(12.0)).get(1).getQuantity());
    }

    /*******************************************
     *
     *   getBucket Test
     *
     *******************************************/

    @Test
    public void getBucket_should_return_correctList()
    {
        initMarket();
        assertTrue(market.getOfferMap().isEmpty());
        market.addOffer("TestUser",new BigDecimal(12.0), 1,"TestCompany");
        market.addOffer("TestUser",new BigDecimal(12.0), 2,"TestCompany");

        // Checks to see if corresponding bucket elements have same quantities.
        assertEquals(1, market.getBucket(market.getOfferMap(), new BigDecimal(12.0)).get(0).getQuantity());
        assertEquals(2, market.getBucket(market.getOfferMap(), new BigDecimal(12.0)).get(1).getQuantity());
    }

    /*******************************************
     *
     *   matchOrders Tests
     *
     *******************************************/

    @Test
    public void bidQuantity_should_correctly_decrement_when_greaterThan_offerQuantity()
    {
        initMarket();
        market.addOffer("TestUser",new BigDecimal(12.0), 6,"TestCompany");
        market.addBid("TestUser",new BigDecimal(12.0), 9,"TestCompany");
        market.matchOrders("TestUser","TestCompany");
        assertEquals(3, market.getBidMap().get(new BigDecimal(12.0)).get(0).getQuantity());  // Bid correctly decremented
        assertTrue(market.getOfferMap().get(new BigDecimal(12.0)).isEmpty());  // Offer correctly closed
    }

    @Test
    public void offerQuantity_should_correctly_decrement_when_greaterThan_bidQuantity()
    {
        initMarket();
        market.addBid("TestUser",new BigDecimal(12.0), 5,"TestCompany");
        market.addOffer("TestUser",new BigDecimal(12.0), 10,"TestCompany");
        market.matchOrders("TestUser","TestCompany");
        assertEquals(5, market.getOfferMap().get(new BigDecimal(12.0)).get(0).getQuantity());  // Offer correctly decremented
        assertTrue(market.getBidMap().get(new BigDecimal(12.0)).isEmpty());  // Bid correctly closed
    }

    @Test
    public void bothQuantities_equal_should_correctly_removeBoth()
    {
        initMarket();
        market.addBid("TestUser",new BigDecimal(12.0), 5,"TestCompany");
        market.addOffer("TestUser",new BigDecimal(12.0), 5,"TestCompany");
        market.matchOrders("TestUser","TestCompany");
        assertTrue(market.getBidMap().get(new BigDecimal(12.0)).isEmpty());   // Bid correctly closed
        assertTrue(market.getOfferMap().get(new BigDecimal(12.0)).isEmpty()); // Offer correctly closed
    }

    @Test
    public void bid_with_ValueAndNoOffers_should_stayTheSame()
    {
        initMarket();
        market.addBid("TestUser",new BigDecimal(12.0), 5,"TestCompany");
        market.matchOrders("TestUser","TestCompany");
        assertEquals(5, market.getBidMap().get(new BigDecimal(12.0)).get(0).getQuantity());   // Bid still has same value
        assertTrue(market.getOfferMap().get(12.0) == null); // Offer still null
    }

    @Test
    public void offer_with_ValueAndNoBids_should_StayTheSame()
    {
        initMarket();
        market.addOffer("TestUser",new BigDecimal(12.0), 5,"TestCompany");
        market.matchOrders("TestUser","TestCompany");
        assertEquals(5, market.getOfferMap().get(new BigDecimal(12.0)).get(0).getQuantity());   // Offer still has same value
        assertTrue(market.getBidMap().get(new BigDecimal(12.0)) == null); // Bid still null
    }

    @Test 
    public void offer_price_higherThan_bidPrice_shouldStayTheSame()
    { 
        initMarket();
        market.addOffer("TestUser",new BigDecimal(12.0), 7,"TestCompany");
        market.addBid("TestUser",new BigDecimal(6.0), 5,"TestCompany");
        market.matchOrders("TestUser","TestCompany");
        assertEquals(7, market.getOfferMap().get(new BigDecimal(12.0)).get(0).getQuantity());   // Offer still has same value
        assertEquals(5, market.getBidMap().get(new BigDecimal(6.0)).get(0).getQuantity());   // Bid still has same value
    }
}
