package com.worldfirst.trade.app.utility;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.worldfirst.trade.app.utility.CurrencyConversionUtils;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
		"currency.conversionrate=1.21",
})
public class CurrencyConversionUtilsTest {

	@Value("${currency.conversionrate}")
	String currencyConversionRate;


	@Autowired
	private CurrencyConversionUtils currencyConversionUtils;
	

	@Test
	public void should_calculate_based_on_currency()
	{
		//when
		String currency="GBP";
		BigDecimal expectedPrice=new BigDecimal(14.52);
	    expectedPrice = expectedPrice.setScale(2, BigDecimal.ROUND_UP);	
		BigDecimal price=new BigDecimal(12);

		BigDecimal actualPrice=	currencyConversionUtils.formattedAmount(currency,price);
		
		//then
		Assert.assertEquals(expectedPrice, actualPrice);
	}

	@Test(expected = NumberFormatException.class)
	public void should_throw_exception(){
		String currency="GBP";
		BigDecimal price=new BigDecimal(Double.NaN);
		currencyConversionUtils.formattedAmount(currency,price);
		
	}  
	
	@Test
	public void should_not_convert_amount_for_USD()
	{
		String currency="USD";
		BigDecimal expectedPrice=new BigDecimal(12);	
		BigDecimal price=new BigDecimal(12);

		BigDecimal actualPrice=	currencyConversionUtils.formattedAmount(currency,price);
		
		//then
		Assert.assertEquals(expectedPrice, actualPrice);
	}
}
