package com.worldfirst.trade.app;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.worldfirst.trade.app.controller.TradingController;
import com.worldfirst.trade.app.model.OrderRequest;
import com.worldfirst.trade.app.model.Type;
import com.worldfirst.trade.app.service.OrderService;

/**
 * @author Nisha.Vaswani
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TradingControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @Mock
	OrderService orderService;
    
    @InjectMocks
	private TradingController underTest = new TradingController();

    @Test 
    public void getHello() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Greetings from Spring Boot!")));
    }

    @Test 
    public void addMarketShare() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/add/share/Testing").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("success")));
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent; 
        } catch (Exception e) {
            throw new RuntimeException(e);
        } 
    }

    @Test
    public void addBid() throws Exception {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setCompanyName("IPO");
        orderRequest.setPrice(new BigDecimal(12.0));
        orderRequest.setQuantity(2);
        orderRequest.setType(Type.BID);
        orderRequest.setUserId("testUser1");
        mvc.perform(MockMvcRequestBuilders.post("/bid/add")
                .content(asJsonString(orderRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("success: bid added.")));
    }
    
    @Test(expected = Exception.class)
	public void add_bid_should_throw_exception() throws Exception,
	Exception {
		OrderRequest orderRequest = new OrderRequest();
		Mockito.doThrow(new Exception()).when( 
				underTest.addBid(orderRequest));

	}

    @Test
    public void addOffer() throws Exception {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setCompanyName("Reliance");
        orderRequest.setPrice(new BigDecimal(78));
        orderRequest.setQuantity(20);
        orderRequest.setType(Type.ASK);
       
        mvc.perform(MockMvcRequestBuilders.post("/ask/add")
                .content(asJsonString(orderRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("success: ask added.")));
    } 
    
    @Test(expected = Exception.class)
    public void add_offer_should_throw_exception() throws Exception,
    Exception {
    	OrderRequest orderRequest = new OrderRequest();
    	Mockito.doThrow(new Exception()).when( 
    			underTest.addOffer(orderRequest));
    }
    
    @Test
    public void matchTrades() throws Exception {      
          mvc.perform(MockMvcRequestBuilders.get("/match/trades")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        underTest.matchOrders();
		//verify service'e method should call once
		Mockito.verify(orderService, times(1)).matchOrder();
		verifyNoMoreInteractions(orderService);          
    }
    
    @Test(expected = Exception.class)
  	public void matchTrades_should_throw_exception() throws Exception,
  	Exception {
  		Mockito.doThrow(new Exception()).when( 
  				underTest.matchOrders());

  	}
    
    @Test
    public void unmatchTrades() throws Exception {      
          mvc.perform(MockMvcRequestBuilders.get("/unmatch/trades")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        underTest.unmatchOrders();
		//verify service'e method should call once
		Mockito.verify(orderService, times(1)).unmatchOrder();
		verifyNoMoreInteractions(orderService);          
    }
    
    
    @Test(expected = Exception.class)
  	public void unmatchTrades_should_throw_exception() throws Exception,
  	Exception {
  		Mockito.doThrow(new Exception()).when( 
  				underTest.unmatchOrders());

  	}
    
    @Test
    public void cancelOffer() throws Exception {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setCompanyName("Reliance");
        orderRequest.setPrice(new BigDecimal(78));
        orderRequest.setQuantity(20);
        orderRequest.setType(Type.ASK);
       
        mvc.perform(MockMvcRequestBuilders.post("/ask/cancel")
                .content(asJsonString(orderRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("success: ask removed.")));
    } 
  
    @Test
    public void cancelBid() throws Exception {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setCompanyName("Reliance");
        orderRequest.setPrice(new BigDecimal(78));
        orderRequest.setQuantity(20);
        orderRequest.setType(Type.BID);
        
        mvc.perform(MockMvcRequestBuilders.post("/bid/cancel")
                .content(asJsonString(orderRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("success: bid removed.")));
    }
}
