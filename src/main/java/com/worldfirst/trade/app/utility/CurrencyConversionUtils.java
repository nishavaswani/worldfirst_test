package com.worldfirst.trade.app.utility;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author Nisha.Vaswani
 *
 */
@PropertySource(ignoreResourceNotFound = true, value = "classpath:config.properties")
@Component
public class CurrencyConversionUtils { 
		
	@Value("${currency.conversionrate}")
	String  currencyConversionRate;
	
	public  BigDecimal formattedAmount(String currency,BigDecimal price ){
		try{
		
		BigDecimal currencyRate=new BigDecimal(currencyConversionRate);
		
		if(currency.equalsIgnoreCase("GBP"))
				{
			BigDecimal amount=price.multiply(currencyRate);
			return amount;
				}
		}
		catch(NumberFormatException e)
		{
		}
		return price;

	}
}
