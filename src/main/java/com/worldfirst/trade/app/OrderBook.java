package com.worldfirst.trade.app;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.worldfirst.trade.app.model.Order;




/**
 * @author Nisha.Vaswani
 *
 */
public class OrderBook {

	private final Logger logger = LoggerFactory.getLogger(OrderBook.class);

	private static Map<BigDecimal, List<Order>> bidMap = null;
	private static Map<BigDecimal, List<Order>> offerMap = null;

	private Queue<BigDecimal> bidMaxPriceList = null;
	private Queue<BigDecimal> offerMinPriceList = null;
	private  String companyName;
	private static List<String> matchTrades = new ArrayList<String>();

	static List<String> unMatchedTrade=new ArrayList<String>();


	public OrderBook(String companyName)
	{
		bidMap = new HashMap<BigDecimal, List<Order>>();
		offerMap = new HashMap<BigDecimal, List<Order>>();


		bidMaxPriceList = new PriorityQueue<BigDecimal>(30, Collections.reverseOrder()); // top is maximum bid price
		offerMinPriceList = new PriorityQueue<BigDecimal>();  // top is minimum offer price
		this.companyName = companyName;
	}

	public void setcompanyNamae(String companyName)
	{
		this.companyName = companyName;
	}
	public void addBid(String userId,BigDecimal price, int quantity,String companyName)
	{
		logger.info("OrderBook user Id name : {}", userId);
		logger.info("OrderBook AddBid price : {}", price);
		logger.info("OrderBook AddBid quantity : {}", quantity);
		logger.info("OrderBook AddBid company Name : {}", companyName);
		List<Order> bucket = getBucket(bidMap, price);
		Order newBid = new Order(price, quantity,companyName,userId);
		bucket.add(newBid); 
		bidMap.put(newBid.getPrice(), bucket);
		bidMaxPriceList.add(price);
		matchOrders(userId,companyName);


	}

	//remove Bid
	public void removeBid(String userId,BigDecimal price, int quantity)
	{
		logger.info("OrderBook removeBid user Id name : {}", userId);
		logger.info("OrderBook removeBid price : {}", price);
		logger.info("OrderBook removeBid quantity : {}", quantity);
		List<Order> bucket = getBucket(bidMap, price);
		Order newBid = new Order(price, quantity,companyName,userId);
		bucket.remove(newBid);
		bidMap.remove(newBid.getPrice());
		for(BigDecimal priceList: bidMaxPriceList)
		{

			bidMaxPriceList.remove(priceList);

		}
		matchOrders(userId,companyName);	
	}

	//remove Offer
	public void removeOffer(String userId,BigDecimal price, int quantity,String companyName)
	{
		logger.info("OrderBook removeAsk user Id name : {}", userId);
		logger.info("OrderBook removeAsk price : {}", price);
		logger.info("OrderBook removeAsk quantity : {}", quantity);
		logger.info("OrderBook removeBid companyName name : {}", companyName);

		List<Order> bucket = getBucket(bidMap, price);
		Order newOffer = new Order(price, quantity,companyName,userId);
		bucket.remove(newOffer);
		offerMap.remove(newOffer.getPrice());

		for(BigDecimal priceList: bidMaxPriceList)
		{

			offerMinPriceList.remove(priceList);

		}
		matchOrders(userId,companyName);
	}

	//add offer
	public void addOffer(String userId,BigDecimal price, int quantity,String companyName)
	{ 
		logger.info("OrderBook addOffer user Id name : {}", userId);
		logger.info("OrderBook addOffer price : {}", price);
		logger.info("OrderBook addOffer quantity : {}", quantity);
		logger.info("OrderBook addOffer companyName name : {}", companyName);

		List<Order> bucket = getBucket(offerMap, price);
		Order newOffer = new Order(price, quantity,companyName,userId);
		bucket.add(newOffer);

		offerMap.put(newOffer.getPrice(), bucket);
		offerMinPriceList.add(price);
		matchOrders(userId,companyName);

	} 

	public Map<BigDecimal, List<Order>> getOfferMap()
	{
		return offerMap;
	}

	public List<Order> getBucket(Map<BigDecimal, List<Order>> hashmap, BigDecimal price)
	{
		List<Order> bucket;
		if(hashmap.containsKey(price))
		{
			bucket = hashmap.get(price);
		}
		else
		{
			bucket = new LinkedList<Order>();
		}
		return bucket;
	}


	public String matchOrders(String userId,String companyName)
	{
		logger.info("OrderBook matchOrders user Id name : {}", userId);
		logger.info("OrderBook matchOrders companyName name : {}", companyName);
		List<Order> bidBucket = null;
		List<Order> offerBucket = null;
		BigDecimal lowestOffer = null;
		BigDecimal highestBid = null;
		boolean finished = false;
		String sucessTrades=null;

		while(!finished)
		{


			// Peek because we don't want to remove the top element until the order is closed
			highestBid = bidMaxPriceList.peek();
			lowestOffer = offerMinPriceList.peek();
			// No possible trade if either list is empty or no bid higher than an offer
			if(lowestOffer == null || highestBid == null || lowestOffer.compareTo( highestBid)>0)
			{
				bidBucket = bidMap.get(bidMaxPriceList.peek());
				offerBucket = offerMap.get(offerMinPriceList.peek());
				if(bidBucket!=null)
				{
					bidBucket.get(0).setCompanyName(companyName);
					bidBucket.get(0).setUserId(userId);
				}
				else if(offerBucket!=null)
				{
					offerBucket.get(0).setCompanyName(companyName);
				}


				finished = true;

			}
			else
			{
				// Gets buckets for both maps
				bidBucket = bidMap.get(bidMaxPriceList.peek());
				offerBucket = offerMap.get(offerMinPriceList.peek());

				// Gets first element from each bucket since they're the oldest
				int bidQuantity = bidBucket.get(0).getQuantity();
				int offerQuantity = offerBucket.get(0).getQuantity();

				bidBucket.get(0).setCompanyName(companyName);
				bidBucket.get(0).setUserId(userId);



				if(bidQuantity > offerQuantity)
				{


					sucessTrades=successfulTrade(userId,companyName,offerQuantity, lowestOffer);

					// Decrements quantity in bid
					bidQuantity -= offerQuantity;
					bidBucket.get(0).setQuantity(bidQuantity);
					bidBucket.get(0).setCompanyName(companyName);
					bidBucket.get(0).setUserId(userId);


					// Closes previous offer
					offerBucket.remove(0);
					offerMinPriceList.remove();
					return sucessTrades;
				}
				else if(offerQuantity > bidQuantity)
				{

					sucessTrades=successfulTrade(userId,companyName,bidQuantity, lowestOffer);

					// Decrements quantity in offer
					offerQuantity -= bidQuantity;
					offerBucket.get(0).setQuantity(offerQuantity);
					bidBucket.get(0).setCompanyName(companyName);
					bidBucket.get(0).setUserId(userId);



					//  Closes previous bid
					bidBucket.remove(0);
					bidMaxPriceList.remove();
					return sucessTrades;
				}
				else
				{
					// bidQuantity is an arbitrary choice because both quantities are equal.
					// lowestOffer is chosen because it's the price at which the trade is made.
					sucessTrades=successfulTrade(userId,companyName,bidQuantity, lowestOffer);
					// Removes bid and offer because they're both closed
					bidBucket.remove(0);
					bidMaxPriceList.remove();
					offerBucket.remove(0);
					offerMinPriceList.remove();
					return sucessTrades;
				}
			}
		}
		return "Not Matched";
	}

	public static List<String> matchOrderLatest()
	{
		return matchTrades;
	} 


	public String successfulTrade(String userId,String companyName,int quantity, BigDecimal price)
	{
		BigDecimal amount=price.multiply(new BigDecimal(quantity));
		String s1= "User: "+ userId + " CompanyName : "+ companyName + " Number of Shares: " +quantity + " shares traded for $" + price + " per share... total amount was :"+ amount + "";
		matchTrades.add(s1);
		return s1;
	}


	// Prints the remaining trades from input map after close of market.
	private static List<String> printFailedTrades(Map<BigDecimal, List<Order>> hashmap, String type)
	{
		List<String> failedTrades=new ArrayList<String>();

		for (BigDecimal key : hashmap.keySet())
		{

			List<Order> bucket = hashmap.get(key);

			for(Order order : bucket)
			{
				BigDecimal amount=order.getPrice().multiply(new BigDecimal(order.getQuantity()));

				failedTrades.add("User: "+ order.getUserId() + " " + type + "CompanyName :"+order.getCompanyName() +" Number of Shares: "+ order.getQuantity() +  " shares for $" + order.getPrice() + " per share failed to trade...total amount was :"+ amount + "" );
			}
		}
		return failedTrades;
	}



	public static List<String> unmatchOrder()
	{

		List<String> failedBid= printFailedTrades(bidMap, "Bid for ");
		List<String> failedOffer=printFailedTrades(bidMap, "offer for");
		unMatchedTrade.addAll(failedBid);
		unMatchedTrade.addAll(failedOffer);
		return  unMatchedTrade;
	}

	public Map<BigDecimal, List<Order>> getBidMap()
	{
		return bidMap;
	}

}




