package com.worldfirst.trade.app;

import java.time.LocalDate;
import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.worldfirst.trade.app.controller.TradingController;

/**
 * @author Nisha.Vaswani
 *
 */
@Configuration
@EnableSwagger2
@ComponentScan(basePackageClasses={TradingController.class},basePackages ={"com.worldfirst.trade.app"})
public class SwaggerConfig {

    @Bean
    public Docket petApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("worldfirst-trade-api")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.worldfirst.trade.app.controller"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, Arrays.asList(new ResponseMessageBuilder()
                        .code(500)
                        .message("500 Internal server error")
                        .responseModel(new ModelRef("Error"))
                        .build()))
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Trade API DOCUMENTATION")
                .description("World First Trade Application Demo.")
                .version("1.0.0")
                .build();
    }

}
