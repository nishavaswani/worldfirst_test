package com.worldfirst.trade.app.model;

import java.math.BigDecimal;


/**
 * @author Nisha.Vaswani
 *
 */
public class Order {

	private String userId;
	private String companyName;
	private BigDecimal price;
	private int quantity;

	public Order(BigDecimal price, int quantity,String companyName,String userId)
	{
		this.price = price;
		this.quantity = quantity;
		this.companyName=companyName;
		this.userId=userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String newuserId) {
		this.userId = newuserId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String newcompanyName) {
		this.companyName = newcompanyName;
	}

	public BigDecimal getPrice()
	{
		return this.price;
	}

	public void setPrice(BigDecimal newPrice)
	{
		this.price = newPrice;
	} 

	public int getQuantity()
	{
		return this.quantity;
	}

	public void setQuantity(int newQuantity)
	{
		this.quantity = newQuantity;
	}

	public String toString()
	{
		return this.price + " " + this.quantity + " " + this.companyName + " " + this.getUserId();
	}
}
