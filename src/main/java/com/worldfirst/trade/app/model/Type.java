package com.worldfirst.trade.app.model;

/**
 * @author Nisha.Vaswani
 *
 */
public enum Type { BID, ASK };

