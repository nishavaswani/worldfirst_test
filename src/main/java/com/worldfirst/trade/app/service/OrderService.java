package com.worldfirst.trade.app.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.worldfirst.trade.app.model.Order;
import com.worldfirst.trade.app.model.OrderRequest;

/**
 * @author Nisha.Vaswani
 *
 */
public interface OrderService {

	public void addBid(OrderRequest bid);

	public void removeBid(OrderRequest bid);

	public void removeAsk(OrderRequest ask);

	public List<String> bidList();

	public void addOffer(OrderRequest offer);

	public Map<BigDecimal,List<Order>> GetOfferMap(OrderRequest offer);

	public void add(String name);

	public List<String> matchOrder();

	public List<String> unmatchOrder();




}
