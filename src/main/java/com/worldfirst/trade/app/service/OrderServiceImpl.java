package com.worldfirst.trade.app.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.worldfirst.trade.app.OrderBook;
import com.worldfirst.trade.app.model.Order;
import com.worldfirst.trade.app.model.OrderRequest;
import com.worldfirst.trade.app.utility.CurrencyConversionUtils;


@PropertySource(ignoreResourceNotFound = true, value = "classpath:config.properties")
@Service
public class OrderServiceImpl  implements OrderService{

	@Value("${currency.conversionrate}")
	String  currencyConversionRate;

	@Autowired
	CurrencyConversionUtils currencyConversionUtils;


	private Map<String, OrderBook> orderBooks = null; 
	private OrderBook market;

	public OrderServiceImpl()
	{
		orderBooks = new HashMap<String, OrderBook>();
		market = new OrderBook("Test");
		orderBooks.put("Test", market);
	}

	@Override
	public void addBid(OrderRequest bid) {
		if (orderBooks.containsKey(bid.getCompanyName())) {
			OrderBook book = orderBooks.get(bid.getCompanyName());
			book.addBid(bid.getUserId(),currencyConversionUtils.formattedAmount(bid.getCurrency(),bid.getPrice()), bid.getQuantity(),bid.getCompanyName());
		}
	}  

	@Override
	public List<String> matchOrder() {
		return	OrderBook.matchOrderLatest(); 
	}

	public List<String> bidList()
	{
		List<String> list = new ArrayList<String>(orderBooks.keySet());  
		return list;
	}

	@Override
	public void addOffer(OrderRequest offer) {
		{
			if (orderBooks.containsKey(offer.getCompanyName())) {
				OrderBook book = orderBooks.get(offer.getCompanyName());

				book.addOffer(offer.getUserId(),currencyConversionUtils.formattedAmount(offer.getCurrency(),offer.getPrice()), offer.getQuantity(),offer.getCompanyName());
			}
		}
	}


	public Map<BigDecimal, List<Order>> GetOfferMap(OrderRequest offer)
	{
		if (orderBooks.containsKey(offer.getCompanyName())) {


			OrderBook book = orderBooks.get(offer.getCompanyName());

			Map<BigDecimal, List<Order>> offerMap = book.getOfferMap();
			return offerMap;
		}
		return null;
	}


	@Override
	public void add(String name) {
		if (!orderBooks.containsKey(name)) {
			market = new OrderBook(name);
			orderBooks.put(name, market);
		}
	}


	@Override
	public List<String> unmatchOrder() {

		return OrderBook.unmatchOrder();
	}

	@Override
	public void removeBid(OrderRequest bid) {
		if (orderBooks.containsKey(bid.getCompanyName())) {
			OrderBook book = orderBooks.get(bid.getCompanyName());
			book.removeBid(bid.getUserId(),currencyConversionUtils.formattedAmount(bid.getCurrency(),bid.getPrice()), bid.getQuantity());	
		}

	}

	@Override
	public void removeAsk(OrderRequest ask) {
		if (orderBooks.containsKey(ask.getCompanyName())) {
			OrderBook book = orderBooks.get(ask.getCompanyName());
			book.removeOffer(ask.getUserId(),currencyConversionUtils.formattedAmount(ask.getCurrency(),ask.getPrice()), ask.getQuantity(),ask.getCompanyName());	
		}

	}

}


