package com.worldfirst.trade.app.controller;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.worldfirst.trade.app.model.OrderRequest;
import com.worldfirst.trade.app.service.OrderService;
import com.worldfirst.trade.app.service.OrderServiceImpl;

/**
 * @author Nisha.Vaswani
 *
 */
@RestController
@RequestMapping("/")
public class TradingController {

	@Autowired
	OrderService orderService;

	private static final Logger LOGGER = LoggerFactory.getLogger(TradingController.class);

	public TradingController()
	{
		orderService=new OrderServiceImpl();
		orderService.add("Test");
	}

	@RequestMapping(value = "add/share/{companyName}", method = RequestMethod.POST)
	public String AddMarketShare(@PathVariable(value="companyName") String companyName) {
		orderService.add(companyName);
		return "success"; 
	} 


	@RequestMapping(value="bid/add",method=RequestMethod.POST,consumes="application/json") 
	public ResponseEntity<String>  addBid(@RequestBody OrderRequest bid) {
		try {
			LOGGER.info("add Bid method"+bid.getUserId());
			orderService.addBid(bid);
		} catch (Exception e) {
			LOGGER.error("Exception in addBid" + e.getMessage());
			throw e;
		}
		return new ResponseEntity<String>("success: bid added.", HttpStatus.OK);
	}  


	@RequestMapping(value = "ask/add", method = RequestMethod.POST,consumes="application/json")
	public ResponseEntity<String> addOffer(@RequestBody OrderRequest offer) {
		try {
			LOGGER.info("add ask method"+offer.getUserId());
			orderService.addOffer(offer);
		} catch (Exception e) {
			LOGGER.error("Exception in addBid" + e.getMessage());
			throw e;
		}
		return new ResponseEntity<String>("success: ask added.", HttpStatus.OK);
	}

	@RequestMapping(value = "bid/cancel", method = RequestMethod.POST,consumes="application/json")
	public ResponseEntity<String> removeBid(@RequestBody OrderRequest bid) {
		try {
			LOGGER.info("cancel bid method"+bid.getUserId());
			orderService.removeBid(bid);
		}
		catch (Exception e) {
			LOGGER.error("Exception in bid cancel" + e.getMessage());
			throw e;
		}
		return new ResponseEntity<String>("success: bid removed.", HttpStatus.OK);
	} 

	@RequestMapping(value = "ask/cancel", method = RequestMethod.POST,consumes="application/json")
	public ResponseEntity<String> removeAskShare(@RequestBody OrderRequest ask) {
		try {
			LOGGER.info("cancel bid method"+ask.getUserId());
			orderService.removeAsk(ask);
		}
		catch (Exception e) {
			LOGGER.error("Exception in ask cancel" + e.getMessage());
			throw e;
		} 

		return new ResponseEntity<String>("success: ask removed.", HttpStatus.OK);
	}

	@RequestMapping(value = "match/trades", method = RequestMethod.GET)
	public List<String> matchOrders() {
		try {
			LOGGER.info("matchOrders  method");
			return orderService.matchOrder();
		}
		catch (Exception e) {
			LOGGER.error("Exception in match trades" + e.getMessage());
			throw e;
		}

	}

	@RequestMapping(value = "unmatch/trades", method = RequestMethod.GET)
	public List<String> unmatchOrders() {
		try {
			LOGGER.info("UnmatchOrders  method");
			return orderService.unmatchOrder();
		}
		catch (Exception e) {
			LOGGER.error("Exception in unmatch trades" + e.getMessage());
			throw e;
		}
	}
 
	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

}



